@if (count($errors) > 0)
  {{-- フォームのエラーリスト --}}
  <div class="alert alert-dismissible alert-warning ">
    <strong>入力エラー</strong>

    <br />

    <ul>
      @foreach ($errors->all() as $error)
        <li>
          {{ $error }}
        </li>
      @endforeach
    </ul>
  </div>
@endif
