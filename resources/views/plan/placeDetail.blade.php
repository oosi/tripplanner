@extends('plan.schedule')

@section('placeDetail')
  <div class="panel panel-default">
    <div class="panel-heading">
      {{$place->name}}
    </div>
    <div class="panel-body">
      <table class="table table-striped">
        <tbody>
          <tr><td><i class="fa fa-clock-o" aria-hidden="true"></i> 到着時刻</td> <td>{{$place->start_time->format('Y/m/d h:i')}}</td></tr>
          <tr><td><i class="fa fa-clock-o" aria-hidden="true"></i> 出発時刻</td> <td>{{$place->end_time->format('Y/m/d h:i')}}</td></tr>
          <tr><td>住所</td> <td>{{$place->location}}</td></tr>
          <tr><td>メモ</td> <td>{{$place->memo}}</td></tr>
        </tbody>
      </table>
      @if($place->latitude != 0 )
        @include('map.placeMap')
      @endif
    </div>
  </div>
@overwrite

@push('scripts')
  <script>
  // コントローラから渡された住所を取得
  // var addressStr = "$address";

  $(document).ready(function(){
    // Gmapsを利用してマップを生成
    var lat = {{$place->latitude}};
    var lng = {{$place->longitude}};
    var map = new GMaps({
      div: '.map',
      lat: lat,
      lng: lng,
      zoom: 15,
    });
    map.addMarker({
      lat: lat,
      lng: lng,
      //      title: '',
    });
    // 住所からマップを表示
    // GMaps.geocode({
    //     address: addressStr.trim(),
    //     callback: function(results, status) {
    //         if (status == 'OK') {
    //             var latlng = results[0].geometry.location;
    //             map.setCenter(latlng.lat(), latlng.lng());
    //         }
    //     }
    // });
  });
  </script>
      @endpush
