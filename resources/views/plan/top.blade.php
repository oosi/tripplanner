@extends('plan.list')

@section('content-main')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        概要
                    </div>
                    <div class="panel panel-body">
                        <i class="fa fa-calendar" aria-hidden="true"></i>
                        {{$plan->start_date->format('Y-m-d')}} ～
                        {{$plan->end_date->format('Y-m-d')}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
