@extends('plan.list')

@section('content-main')
  <div class="container-fluid" style="margin-bottom: 15px;">
    @include('components.flash')

    {{-- メニューバーを作ってみたが現時点では不要？また使うかも
     <div class="bs-component" style="margin-bottom: 15px;">
      <div class="btn-group btn-group-justified btn-group-raised">
        <a class="btn ">概要</a>
        <a class="btn" v-on:click="changeAddMode" v-bind:class="{active: addMode}"  style="margin-bottom:20px;">
          <i class="fa fa-sticky-note-o" aria-hidden="true"></i> 新しい予定
        </a>
        <a class="btn">検討リスト</a>
        <a class="btn">全体地図</a>
      </div>
    </div> --}}

  </div>
  <div class="container-fluid" id="placeDetail">
    <div class="row">
      {{-- 左ペイン(スケジュール一覧) --}}
      <div class="col-sm-4 col-md-4">
        <!-- 既存の予定を一覧表示 -->
        <div class="panel panel-success">
          <div class="panel-heading clearfix" style="padding-bottom: 0px;">
            <p class="pull-left">スケジュール</p>
          </div>
          <div class="panel-body">

            {{-- リスト表示 予定が作成されているとき --}}
            @if (!$places->isEmpty())

              @foreach ($places as $place)
                {{-- 最初にテーブルの記述と日付を保持 --}}
                @if ($loop->first)
                  <h4><i class="fa fa-calendar " aria-hidden="true"></i> {{$place->start_time->format('Y/m/d')}}</h4>
                  <table class="table table-responsive">
                    <tbody>
                    <?php $i = 0; ?>
                    <?php $dt = $place->start_time ?>
                @endif
                {{-- 日付が変わったらテーブルとヘッダを作り直す --}}
                @if(!$place->start_time->isSameDay($dt))
                  @if($place->start_time->year === $dt->year)
                    <h4><i class="fa fa-calendar" aria-hidden="true"></i>
                      {{$place->start_time->format('m/d')}}
                    </h4>
                  @else
                    <h4><i class="fa fa-calendar" aria-hidden="true"></i>
                      {{$place->start_time->format('Y/m/d')}}
                    </h4>
                  @endif
                  <?php $dt = $place->start_time ?>
                @endif

                {{--------- 日時と行き先表示 ----------}}
                  <tr v-on:click="showDetail{{$i}}" class="btn col-xs-12" style="margin:0px;padding:0px;">
                    <td class="col-xs-4">
                      <h4 style="padding:0px;margin:0px;"></i>
                        {{$place->start_time->format('h:i')}}
                      </h4>
                      {{-- 終了時刻がない場合にも対応したい --}}
                      <div class="text-center">
                        <i class="fa fa-caret-down " aria-hidden="true"></i>
                      </div>
                      <p>{{$place->end_time->format('G:i')}}</p>
                    </td>
                    <td class="col-xs-12 text-center" >
                      <h4>{{ $place->name}}</h4>
                    </td>
                    <?php $i++; ?>
                  </tr>

                @if ($loop->last)
                  </tbody>
                </table>
              @endif
              @endforeach
                {{-- 予定がないとき --}}
              @else
                <p class="panel-info">予定が登録されていません。「新しい予定」から予定を追加しましょう！</p>
              @endif
              <a class="btn btn-primary" v-on:click="changeAddMode" v-bind:class="{active: addMode}"  style="margin-bottom:20px;">
                <i class="fa fa-sticky-note-o" aria-hidden="true"></i> 新しい予定</a>
            </div>
          </div>
          {{-- <a href="{{action('PlaceController@drawRoute',['plan' => $plan->id])}}" class="btn btn-raised btn-info">ルート検索</a> --}}
        </div> {{--end col-sm-4 (左ペイン終わり) --}}

        {{-- 右ペイン--}}
        <div class="col-sm-8 col-md-8">
          {{---------------- '予定表示'  ------------}}
          <div class="container-fluid">
            <template v-if="viewMode">
              <div id="edit">
                <div class="panel panel-primary">
                  <div class="panel-heading clearfix" style="padding-bottom: 0px;" >
                    <p>行き先詳細</p>
                  </div>
                  <div class="panel-body" style="padding-top: 15px;">
                    <p v-if="editvm.editMode" class="text-primary">項目を編集して「更新」を押してください</p>
                    <form v-bind:action="modifyURL" method="POST" class="form-horizontal">
                      {{csrf_field() }}
                      <table class="table table-striped">
                        <tr style="height:45px;">
                          <td><i class="fa fa-clock-o" aria-hidden="true"></i>名前</td>
                          {{-- editvm.editModeがtrueのとき普通に描画 --}}
                          <td v-if="!editvm.editMode" v-cloak>@{{placeName}}</td>
                          <td v-if="editvm.editMode"><input type="text"  type="text" name="placeName" v-model="placeName" class="form-control"/></td>
                        </tr>
                        {{-- コンポーネントのことがわかったら、timepickerを要素化してここでも起動する・・・。--}}
                        <tr>
                          <td><i class="fa fa-clock-o" aria-hidden="true"></i> 到着時刻</td>
                          <td v-if="!editvm.editMode" v-cloak>@{{startTime}}</td>
                          <td v-if="editvm.editMode"><input type="text" type="text" name="startTime" v-model="startTime" class="form-control timepicker"/></td>
                        </tr>
                        <tr>
                          <td><i class="fa fa-clock-o" aria-hidden="true"></i> 出発時刻</td>
                          <td v-if="!editvm.editMode" v-cloak>@{{endTime}}</td>
                          <td v-if="editvm.editMode"><input type="text"  type="text" name="endTime" v-model="endTime" class="form-control timepicker"/></td>
                        </tr>
                        <tr>
                          <td>住所</td>
                          <td v-if="!editvm.editMode" v-cloak>@{{location}}</td>
                          <td v-if="editvm.editMode"><input type="text"  type="text" name="location" v-model="location" class="form-control"/></td>
                        </tr>
                        <tr>
                          <td>メモ</td>
                          <td v-if="!editvm.editMode" v-cloak>@{{memo}}</td>
                          <td v-if="editvm.editMode"><input type="text"  type="text" name="memo" v-model="memo" class="form-control"/></td>
                        </tr>
                      </table>
                      {{-- <button v-if="!editvm.editMode" type="btn" class="btn btn-link disabled pull-right" value="#"></button> --}}
                      <button v-if="editvm.editMode" type="btn" class="btn btn-primary pull-right" value="submit">更新</button>
                    </form>
                    {{-- <span class="label label-info">MAP</span> --}}
                    {{-- <span><br/>&nbsp;</span> --}}
                    <div v-html="viewMapOrNotice"></div>

                    {{---------------編集・地図更新・削除ボタン------------}}
                    <div class="bs-component" style="margin-bottom: 15px;">
                      <form v-bind:action="deleteURL" method="POST">
                        <div class="btn-group btn-group-justified btn-group-raised">
                          {{-- <a v-bind:href='deleteURL' class="btn btn-danger">削除</a> --}}
                          <a v-on:click="editvm.editMode = true" class="btn btn-primary" v-if="!editvm.editMode">編集</a>
                          <a v-on:click="editvm.editMode = false" v-else class="btn btn-warning">キャンセル</a>
                          <a v-bind:href='reloadMapURL' class="btn btn-success pull-center" v-bind:class="{disabled: editvm.editMode}">地図の更新</a>
                          {{ csrf_field() }}
                          {{ method_field('DELETE') }}
                          <a onclick="this.parentNode.parentNode.submit();return false;" href="#" class="btn btn-danger" v-bind:class="{disabled: editvm.editMode}">削除</a>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </template>
            {{--------------------新しい予定--------------------}}
            <template v-if="addMode">
          <div class="panel panel-primary">
            <div class="panel-heading clearfix" style="padding-bottom: 0px;">
              <p>新しい予定</p>
            </div>
            <div class="panel-body">
              {{-- @include('common.errors') --}}

              <form action="{{ route('plan',['plan' => $plan->id]).'/schedule' }}" method="POST" class="form-horizontal">
                {{csrf_field() }}
                {{-- <a href="/plan/{{ $plan->id }}">{{ $plan->name }}</a><br /> --}}

                <div class="form-group">
                  <label for="place-name" class="col-sm-3 control-label">
                    行き先
                  </label>

                  <div class="col-sm-6">
                    <input type="text" name="name" id="place-name" class="form-control" />
                  </div>
                </div>
                {{-- 日付入力 --}}
                <div class="form-group">
                  <label for="plan-date" class="col-sm-3 control-label">日付</label>
                  <div class="col-sm-7">
                    <input type="text" name="date" class="form-control flatpickr" value="{{$plan->start_date->format('Y-m-d')}}" requred />
                  </div>
                </div>
                {{-- 時刻入力 --}}
                <div class="form-group">
                  <label for="plan-start-date" class="col-sm-3 control-label">到着時刻</label>
                  <div class="col-sm-7">
                    <input type="text" name="startTime" class="form-control timepicker" maxlength="8" value="09:00" requred />
                    {{-- <input id="tp1" maxlength="8" type="text" /> --}}
                  </div>
                </div>
                <div class="form-group">
                  <label for="plan-end-date" class="col-sm-3 control-label">出発時刻</label>
                  <div class="col-sm-7">
                    <input type="text" name="endTime" class="form-control timepicker" maxlength="8" value="09:00" requred />
                  </div>
                </div>
                <div class="form-group">
                  <label for="plan-name" class="col-sm-3 control-label">住所</label>
                  <div class="col-sm-7">
                    <input type="text" name="address" id="address" class="form-control" value=""  />
                  </div>
                </div>
                <div class="form-group">
                  <label for="plan-name" class="col-sm-3 control-label">メモ</label>
                  <div class="col-sm-7">
                    <input type="text" name="memo" id="memo" class="form-control" value=""  />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-xs-offset-7 col-xs-6">
                    <button type="submit" class="btn btn-default btn-primary clearfix" >
                      <i class="fa fa-plus "></i>
                      追加
                    </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </template>
      </div>
    </div> {{-- end col-sm-8 --}}
  </div>

@endsection

@push('scripts')
  <script type="text/javascript">
  $(function() {
    $('.timepicker').timepicker({
      defaultTime: 'value',
      showMeridian: false,
      disableFocus: true,
    });
    $(".flatpickr").flatpickr({
      "locale": "ja"
    });
  });
  </script>

  @include('plan.schedule_script')
@endpush
