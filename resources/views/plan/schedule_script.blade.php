
<script>
var vm = new Vue({
  el: '#placeDetail',
  data: {
    addMode: true,
    viewMode: false,
    placeName: '',
    startTime: '',
    endTime: '',
    location:'',
    memo:'',
    viewMapOrNotice:'',
    reloadMapURL:'',
  },
  //マップを読み込むコールバック
  created: function() {
    this.$on('loadMap', function(lat,lng,name){
      var map = new GMaps({
        div: '.map',
        lat: lat,
        lng: lng,
        zoom: 14,
      });
      map.addMarker({
        lat: lat,
        lng: lng,
        title:  name,
      });
    });
  },
  methods: {
    changeAddMode: function() {
      this.addMode = true;
      this.viewMode = false;
      $.material.init();
    },
    @foreach ($places as $place)
    @if ($loop->first)
    <?php $i = 0; ?>
    @endif
    showDetail{{$i}}: function() {
      this.addMode = false;
      this.viewMode = true;
      this.placeName = '{{$place->name}}';
      this.startTime = '{{$place->start_time->format('Y/m/d G:i')}}';
      this.endTime = '{{$place->end_time->format('Y/m/d G:i')}}';
      this.location = '{{$place->location}}';
      this.memo = '{{$place->memo}}';
      {{-- ボタンのURL --}}
      this.modifyURL =  "{{action('PlaceController@update', ['plan' => $plan->id, 'place' => $place->id ])}}";
      this.reloadMapURL = "{{ action('PlaceController@getCoordinate', ['plan' => $plan->id, 'place' => $place->id ])}}";
      this.deleteURL = "{{action('PlaceController@destroy', ['plan' => $plan->id, 'place' => $place->id ])}}";

      {{-- 座標保持時に地図表示 --}}
      @if($place->latitude != 0 && $place->longitude != 0)
        this.viewMapOrNotice =
        '<div class="map-embed" style="margin-top: 80px; padding : 0px;height : 300px;"><div class="map" ></div></div>';

          this.$emit('loadMap',{{$place->latitude}},{{$place->longitude}},'{{$place->name}}');

      @else
        this.viewMapOrNotice = '<span>座標が取得されていません<span>';
      @endif

    },
    <?php $i++ ?>
    @endforeach
  }
});

// vm.$watch('addMode',  function(value, mutation) {
//   alert(vm.addMode);
//   // alert('1');
// });

Vue.nextTick(function () {
  $.material.init();
});

var editvm = new Vue({
  el: '#edit',
  data: {
    editMode : false,
  }
});

</script>
