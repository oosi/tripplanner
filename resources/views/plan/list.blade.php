@extends('layouts.app')

{{-- 地図表示はこれ --}}
{{-- @include('map.placeMap') --}}

@section('header')
    <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js'></script>
    <link href="{{asset('css/map.css')}}" rel="stylesheet">  {{-- google map --}}
@endsection

@section('content')
    <div class="container-fluid">
      <h2 class="page-header" style="margin-top:7px;margin-bottom:13px;">{{ $plan->name}}</h2>
    </div>
    <div class="container-fluid" style="height:60px;">
        <ul class="nav nav-pills">
            <!-- メニューバー -->
            <li><a class="btn {{ Request::is('*/top') ? 'active' : '' }}" href="{{action('PlaceController@index',['plan' => $plan->id])}}">トップ</a></li>
            <li><a class="btn {{ Request::is('*/schedule*') ? 'active' : '' }}" href="{{action('PlaceController@schedule',['plan' => $plan->id])}}">スケジュール</a></li>
            {{-- 全体マップは予定が登録されていない場合無効か --}}
            @if(!$places->isEmpty())
                <li><a class="btn {{ Request::is('*/route*') ? 'active' : '' }}" href="{{action('PlaceController@drawRoute',['plan' => $plan->id])}}">全体マップ</a></li>
            @else
                <li><a class="btn disabled" href="#">全体マップ</a></li>
            @endif
            {{-- <li><a class="btn {{ Request::is('*/search-route*') ? 'active' : '' }}" href="{{action('PlaceController@searchRoute',['plan' => $plan->id])}}">おすすめルート</a></li> --}}
        </ul>
        <hr />
    </div>
    @yield('content-main')
@endsection

@push('scripts')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBV5ZLsn40Uj3Y7UDX4HBGLQORUpw1uS4Y" ></script>
    <script type="text/javascript" src="{{asset('/assets/bower/gmaps/gmaps.min.js')}}"></script>
@endpush
