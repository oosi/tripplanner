@extends('layout')

@section('content')

  @extends('layouts.app')

  @section('header')
      <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.8.1/jquery.min.js'></script>
      <script type="text/javascript" src="bootstrap.min.js"></script>
      <script type="text/javascript" src="bootstrap-datetimepicker.min.js"></script>
      {{-- timepicker --}}
      {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" />
      <script src="https://code.jquery.com/jquery-3.1.1.min.js"   integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="   crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/locales/bootstrap-datepicker.ja.min.js"></script> --}}
      <link href="{{asset('css/sidebar.css')}}" rel="stylesheet">
  @endsection

  @section('content')
      <div class="container-fluid">
          <div class="row">
              <div class="col-sm-8">
                  <div class="panel panel-default">
                      <div class="panel-heading">
                          {{ $plan->name}}
                      </div>
                      <div class="panel-body">
                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  概要
                              </div>
                              <div class="panel panel-body">
                                  <i class="fa fa-calendar" aria-hidden="true"></i>
                                  {{$plan->start_date->format('Y-m-d')}} ～
                                  {{$plan->end_date->format('Y-m-d')}}
                              </div>
                          </div>

                          <div class="panel panel-default">
                              <div class="panel-heading">
                                  新しい予定を追加
                              </div>
                              <div class="panel-body">
                                  @include('common.errors')

                                  <form action="{{ Request::url() }}" method="POST" class="form-horizontal">
                                      {{csrf_field() }}
                                      {{-- <a href="/plan/{{ $plan->id }}">{{ $plan->name }}</a><br /> --}}

                                      <div class="form-group">
                                          <label for="place-name" class="col-sm-3 control-label">
                                              行き先
                                          </label>

                                          <div class="col-sm-6">
                                              <input type="text" name="name" id="place-name" class="form-control" />
                                          </div>
                                      </div>

                                      {{-- 日付入力 --}}
                                      <div class="form-group">
                                          <label for="plan-date" class="col-sm-3 control-label">日付</label>
                                          <div class="col-sm-7">
                                              <input type="date" name="date" id="place-date" class="form-control " value="{{$plan->start_date->format('Y-m-d')}}" requred />
                                          </div>
                                      </div>

                                      {{-- 時刻入力 --}}
                                      <div class="form-group">
                                          <label for="plan-name" class="col-sm-3 control-label">到着時刻</label>
                                          <div class="col-sm-7">
                                              <input type="time" name="startTime" id="place-start-time" class="form-control" value="09:00" requred />
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <label for="plan-name" class="col-sm-3 control-label">終了時刻</label>
                                          <div class="col-sm-7">
                                              <input type="time" name="endTime" id="place-end-time" class="form-control" value="09:00" requred />
                                          </div>
                                      </div>

                                      <div class="form-group">
                                          <div class="col-sm-offset-8 col-sm-6">
                                              <button type="submit" class="btn btn-default btn-primary">
                                                  <i class="fa fa-plus "></i>
                                                  追加
                                              </button>
                                          </div>
                                      </div>
                                  </div>
                              </form>
                          </div>

@endsection
