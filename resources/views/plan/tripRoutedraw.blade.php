@extends('plan.list')

@section('content-main')
  <div class="container">
  <div class="panel panel-default">
    <div class="panel-heading">
      全体地図
      <?php  //dd($places->all()); ?>
    </div>
    <div class="panel-body">
      <table class="table table-striped">
        <tbody>
        </tbody>
      </table>
      <div class="panel-body">
        <div class="map-embed" style="height : 250px">
          <div class="map" >地図</div>
        </div>
      </div>
    </div>
  </div>
</div>
@overwrite

@push('scripts')
<script>
  // コントローラから渡された住所を取得
  // var addressStr = "$address";
  $(document).ready(function(){
    // Gmapsを利用してマップを生成
    var lat = {{$places->first()->latitude}};
    var lng = {{$places->first()->longitude}};
    var map = new GMaps({
      div: '.map',
      lat: lat,
      lng: lng,
      zoom: 13,
    });
    var waypts = [];
    @foreach($places as $place)
    map.addMarker({
      lat: {{$place->latitude}},
      lng: {{$place->longitude}},
      infoWindow: {
        content: '<div class="overlay">{{$place->name}}</div>'
      }
    });
    {{-- 最初と最後以外を中間地点とする --}}
    @if (!($loop->first || $loop->last))
    waypts.push({
      location: "{{$place->latitude}},{{$place->longitude}}",
      stopover: true
    });
    @endif
    @endforeach

    map.drawRoute({
      origin: [map.markers[0].getPosition().lat(),
      map.markers[0].getPosition().lng()],
      destination: [map.markers[map.markers.length-1].getPosition().lat(),
      map.markers[map.markers.length-1].getPosition().lng()],
      waypoints: waypts,
      // travelMode: 'driving',
      strokeColor: '#FF3300',
      strokeOpacity: 0.5,
      strokeWeight: 6
    });
    // alert('test');
  });
    // 住所からマップを表示
    // GMaps.geocode({
    //     address: addressStr.trim(),
    //     callback: function(results, status) {
    //         if (status == 'OK') {
    //             var latlng = results[0].geometry.location;
    //             map.setCenter(latlng.lat(), latlng.lng());
    //         }
    //     }


    </script>
      @endpush
