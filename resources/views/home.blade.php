@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-offset-2 col-sm-8">
                @include('common.errors')
                <div class="form-group">
                    <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#newPlanModal">
                        新規
                    </button>
                </div>
            </div>
            <div class="col-sm-offset-2 col-sm-8">
                <div class="panel panel-primary">
                    <div class="panel-heading">プラン一覧</div>

                    <div class="panel-body">
                        <table class="table table-striped task-table">
                            {{-- <thead>
                                <th>プラン一覧</th>
                            </thead> --}}
                            <tbody>
                                @foreach ($plans as $plan)
                                    <tr>
                                        <td class="table-text">
                                            <a href="{{ action('PlaceController@index',['plan' => $plan->id]) }}">{{ $plan->name }}</a><br />
                                        </td>
                                        <td class="table-text">
                                            {{-- <a href="/plan/{{ $plan->id }}">{{ $plan->name }}</a><br /> --}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="table-text">
                                            {{-- 時間 --}}
                                            <i class="fa fa-calendar" aria-hidden="true"></i>{{' '. $plan->start_date->format('Y年m月d日')}}
                                            @if ($plan->end_date != $plan->start_date)
                                                ～
                                                @if ($plan->start_date->year != $plan->end_date->year)
                                                    {{ $plan->start_date->year .'年'}}
                                                @endif
                                                {{ $plan->end_date->format('m月d日')}}

                                            @endif
                                        </td>
                                        <td>
                                            <form class="delete" action="{{action('PlanController@destroy',['plan' => $plan->id])}}") method="POST">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}

                                                <button type="submit" id="delete-plan-{{ $plan->id }}" class="btn btn-primary pull-right">
                                                    <i class="fa fa-btn fa-trash"></i>delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            {{-- 新規予定作成 --}}
            <div class="col-sm-offset-2 col-sm-8">
            </div>
        </div>
    </div>

    <div class="modal fade" id="newPlanModal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                {{-- 新規計画フォーム --}}
                <form action="{{ url('home')}}" method="POST" class="form-horizontal">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"></button>
                        <h4 class="modal-title">新しく計画をたてる</h4>
                    </div>
                    <div class="modal-body">

                        {{-- 旅の名前 --}}
                        <div class="form-group">
                            <label for="plan-name" class="col-sm-2 control-label">旅行名</label>
                            <div class="col-sm-9">
                                <input type="text" name="name" id="plan-name" class="form-control" value="{{ old('plan')}}" />
                            </div>
                        </div>
                        {{-- 日程 --}}
                        <div class="form-group">
                            <label for="plan-name" class="col-sm-2 control-label">期間</label>
                            <div class="col-sm-6">
                                <input type="text" name="date" class="form-control datapicker" value="{{date("Y-m-d")}} to {{date("Y-m-d")}}" requred />
                            </div>
                        </div>
                        {{-- <div class="form-group">
                            <label for="plan-name" class="col-sm-2 control-label">終了日</label>
                            <div class="col-sm-6">
                                {{-- <input type="date" name="endDate" id="plan-end-date" class="form-control datepicker" value="{{date("Y-m-d")}}" requred />
                            </div>
                        </div> --}}

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
                        <button type="submit" class="btn btn-default btn-primary"><i class="fa fa-btn fa-plus"></i>作成</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
    $(function() {
        $(".datapicker").flatpickr({
            "mode": "range",
            "locale": "ja"
        });
    });
    </script>
@endpush

{{--
@push('scripts')
  <script type="text/javascript">
  $(function() {
    $(".flatpickr").flatpickr({
    });
  });
  </script>

  @include('plan.schedule_script')
@endpush


--}}
