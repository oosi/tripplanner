@extends('layouts.app')

@section('header')
<link href="{{asset('css/map.css')}}" rel="stylesheet">
@endsection

@section('content')
  <div class="container">
    <div class="col-sm-offset-2 col-sm-10-fluid">
      <div class="panel panel-default">
        <div class="panel-heading">
          地図
        </div>
        <div class="panel-body">
          <div class="map-embed">
            <div id="map-canvas">ここに地図が表示されます</div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@push('scripts')
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
  {{-- <script src="{{asset('js/map.js')}}"></script> --}}
<script>
  $(function() {
      var address = "181-0013";
      new google.maps.Geocoder().geocode({'address': address}, callbackRender);
          // Geocoder.geocode 関数に住所とコールバック関数を渡す
  });

  /**
   * ジオコーダの結果を取得したときに実行するコールバック関数。
   *
   * この関数内で GoogleMap を出力する。
   *
   * @param results ジオコーダの結果
   * @param status ジオコーディングのステータス
   *
   */
  function callbackRender(results, status) {
      if(status == google.maps.GeocoderStatus.OK) {
          var options = {
              zoom: 18,
              center: results[0].geometry.location, // 指定の住所から計算した緯度経度を指定する
              mapTypeId: google.maps.MapTypeId.ROADMAP // 「地図」で GoogleMap を出力する
          };
          var gmap = new google.maps.Map(document.getElementById('map-canvas'), options);
              // #map-canvas に GoogleMap を出力する
          new google.maps.Marker({map: gmap, position: results[0].geometry.location});
              // 指定の住所から計算した緯度経度の位置に Marker を立てる

          adjustMapSize();
      }
  }

  /**
   * GoogleMap を表示する部分のサイズを調整する。
   *
   */
  function adjustMapSize() {
      var mapCanvas = $('#map-canvas');
      var marginBottom = 5; // CSS に定義してある margin の値
      mapCanvas.css("height", ($(window).height() - mapCanvas.offset().top - marginBottom) + "px");
  }
  </script>
@endpush
