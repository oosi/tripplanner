{{--フラッシュメッセージを表示--}}
@if (Session::has('flash_message'))
  <div class="alert alert-dismissible alert-info" style="border-radius: 2px;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <p style="font-size:110%">{!! Session::get('flash_message') !!}</p>
  </div>
@endif
