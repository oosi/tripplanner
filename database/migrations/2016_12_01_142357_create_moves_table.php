<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moves', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('transportation'); //交通手段
            $table->integer('from_place_id')->unsigned(); //from
            $table->integer('to_place_id')->unsigned(); //to
            $table->integer('required_time')->unsigned()->nullable(); //所要時間(min)
            $table->text('memo')->nullable();

            //placesテーブルのカラム削除に合わせて関連する移動情報も削除
            $table->foreign('from_place_id')->references('id')->on('places')->onDelete('cascade');
            $table->foreign('to_place_id')->references('id')->on('places')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moves');
    }
}
