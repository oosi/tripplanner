<?php

use Illuminate\Database\Seeder;

class PlansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Plan::class,3)->create()->each(function($u) {
        $u->places()->save(factory(App\Place::class)->make());
      });
    }
}
