<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|

*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    $faker = Faker\Factory::create('ja_JP');

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Plan::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ja_JP');

    return [
        'name' => $faker->word,
        'user_id' => $faker->unique()->numberBetween($min = 1, $max = 6),
    ];
});

$factory->define(App\Place::class, function (Faker\Generator $faker) {
    $faker = Faker\Factory::create('ja_JP');

    return [
        'plan_id' => $faker->numberBetween($min = 1, $max = 12),
        'name' => $faker->word,
        'start_time' => $faker->dateTimeBetween(
            $startDate = '+ 2 years',
            $endDate = '+ 2 years + 1 day ',
            $timezone = 'Asia/Tokyo'),
        // 'end_time' => $faker->dateTimeBetween(
        //     $startDate = $endDate,
        //     $endDate = '+ 2 years + ',
        //     $timezone = 'Asia/Tokyo'),
        'location' => $faker->streetAddress,
        'memo' => $faker->sentence($nbWords = 6, $variableNbWords = true),
    ];
});
