<?php
namespace Deployer;
require 'recipe/laravel.php';

// Configuration

set('repository', 'git@domain.com:username/repository.git');

add('shared_files', []);
add('shared_dirs', []);

add('writable_dirs', []);

// Servers

server('production', '49.212.180.204')
    ->user('macaclash')
    ->password('7pw9yaghsy')
    identityFile()
    ->set('deploy_path', '/var/www/domain.com');


// Tasks

desc('Restart PHP-FPM service');
task('php-fpm:restart', function () {
    // The user must have rights for restart service
    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
    run('sudo systemctl restart php-fpm.service');
});
after('deploy:symlink', 'php-fpm:restart');

task('my_task', function() {
    write('<comment>executing my task!</comment>');
})->desc('タスクの説明');
