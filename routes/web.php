<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'WelcomeController@index');

 Auth::routes();

//ユーザのプラン全体を表示
Route::get('/home', 'PlanController@index');

//場所の削除
Route::delete('/plan/{plan}/schedule/{place}', 'PlaceController@destroy');


//リダイレクトと/plan/{plan}/をroute(plan)で表示できるようにする
Route::get('/plan/{plan}/',function($plan) {
    return redirect('/plan/'.$plan.'/top');
})->name('plan');

//プランのトップページを表示
Route::get('/plan/{plan}/top', 'PlaceController@index');

//スケジュール一覧を表示
Route::get('/plan/{plan}/schedule', 'PlaceController@schedule');
//行き先の追加
Route::post('/plan/{plan}/schedule', 'PlaceController@add');
// //行き先詳細
// Route::get('/plan/{plan}/schedule/{place}','PlaceController@showPlaceDetail');
//行き先情報の更新
Route::post('/plan/{plan}/schedule/{place}','PlaceController@update');
//行き先の座標再取得
Route::get('/plan/{plan}/schedule/{place}/reloadMap',
'PlaceController@getCoordinate');


//Route::get('/home', 'HomeController@index');
//スケジュール通りのルート表示
Route::get('/plan/{plan}/route','PlaceController@drawRoute');
//ルート探索
Route::get('/plan/{plan}/search-route','PlaceController@searchRoute');
//--------------------------------------
//プラン追加
Route::post('/home', 'PlanController@add');

//プランの削除
Route::delete('/plan/{plan}', 'PlanController@destroy');


Route::get('/map', 'PlaceController@viewMap');
