<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Move extends Model
{
    protected $fillable = ['name', 'transportation','from_place_id','to_place_id',
    'required_time','memo'];

    $table->increments('id');
    $table->timestamps();
    $table->string('transportation')->nullable(); //交通手段
    $table->integer('from_place_id')->unsigned(); //from
    $table->integer('to_place_id')->unsigned(); //to
    $table->integer('required_time')->unsigned()->nullable(); //所要時間(min)
    $table->text('memo')->nullable();

    public function place() {
        return $this->belongTo('App\Move','to_place_id','from_place_id'); //to_place_idを外部キーとする
     }

}
