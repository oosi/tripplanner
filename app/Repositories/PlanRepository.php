<?php

namespace App\Repositories;

use App\User;
use App\Plan;

class PlanRepository
{
    /**
     * 指定ユーザーのプラン取得
     *
     * @param  User  $user
     * @return Collection
     */
    public function forUser(User $user)
    {
      return Plan::where('user_id', $user->id)
                  ->orderBy('created_at', 'asc')
                  ->get();

        // return Plan::where('user_id, $user->id')$user->plans()
        //             ->find(1) //暫定
        //             ->places()
        //             ->orderBy('created_at', 'asc')
        //             ->get();
    }
}
