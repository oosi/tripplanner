<?php

namespace App\Repositories;

use App\Plan;
use App\Place;


class PlaceRepository
{
    /**
     * 指定プランの全行き先取得
     *
     * @param  User  $place
     * @return Collection
     */
    public function forPlan(Plan $plan)
    {
        return Place::where('plan_id', $plan->id)
                    ->orderBy('start_time', 'asc')
                    ->get();

        // return $plan->places()
        //             ->orderBy('created_at', 'asc')
        //             ->get();
    }
}
