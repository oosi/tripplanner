<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    protected $fillable = ['name','start_time','end_time','location',
    'memo','latitude','longitude'];
    protected $dates = ['start_time','end_time'];



    public function plan() {
        return $this->belongsTo(Plan::class);
    }

    public function move() {
      return $this->hasOne('App\Move');
    }
}
