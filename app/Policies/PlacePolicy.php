<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlacePolicy
{
  use HandlesAuthorization;

  /**
  * Create a new policy instance.
  *
  * @return void
  */
  public function destroy(Plan $plan, Place $place)
  {
    return $plan->id === $place->plan_id;
  }

}
