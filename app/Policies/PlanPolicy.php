<?php

namespace App\Policies;

use App\User;
use App\Place;
use Illuminate\Auth\Access\HandlesAuthorization;

class PlanPolicy
{
    use HandlesAuthorization;

    /**
     * 指定されたユーザーが指定されたタスクを削除できるか決定
     *
     * @param  User  $user
     * @param  Plan  $plan
     * @return bool
     */
    public function destroy(User $user, Plan $plan)
    {
        return $user->id === $plan->user_id;
    }
}
