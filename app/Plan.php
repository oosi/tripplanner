<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    //複数代入
          protected $fillable = ['name','start_date','end_date'];
          protected $dates = ['start_date','end_date'];

          public function user() {
            return $this->belongsTo(User::class);
          }

          public function places() {
            return $this->hasMany(Place::class);
          }
}
