<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlaceRepository;
use App\Http\Requests;
use App\Plan;
use App\Place;
use Carbon\Carbon;

class PlaceController extends Controller
{
  // 行き先リポジトリーインスタンス
  // @var PlaceRepository
  protected $places;

  // 新しいコントローラーインスタンスの生成
  // @param PlanRepository $plans
  // @return void
  public function __construct(PlaceRepository $places) {
    $this->middleware('auth');
    $this->places = $places;
    // dd($places);
  }

  //-----------------ユーザの全行き先を取得-----------------
  public function index(Request $request, Plan $plan) {
    return view('plan.top', [
      'places' => $this->places->forPlan($plan),
      'plan' => $plan,
    ]);
  }

  //--------------ユーザの全行き先を表示---------------
  //@param Request $request
  //@return Response
  public function schedule(Request $request, Plan $plan) {
    // dd($this->places->forPlan($request->plan));
    return view('plan.schedule', [
      'places' => $this->places->forPlan($plan),
      'plan' => $plan,
    ]);
  }

  //---------------------行き先の詳細を表示----------------------
  // public function showPlaceDetail(Request $request,Plan $plan,Place $place) {
  //   return view('plan.placeDetail', [
  //     'places' => $this->places->forPlan($plan),
  //     'place' => $place,
  //     'plan' => $plan,
  //   ]);
  // }

  //--------------------行き先の追加---------------------------
  public function add(Request $request, Plan $plan) {
    $this->validate($request, [
      'name' => 'required|max:255',
      'startTime' => 'required',
      // 'endTime' => 'required|after:startTime',
    ]);

    // try {
      // 入力からy-m-d h:m:s形式(Carbon)の予定時刻をつくる
      $startDateTime = Carbon::createFromFormat('Y-m-d H:i:s',
      $request->date.' '.$request->startTime.':00')->toDateTimeString();

      $endDateTime = Carbon::createFromFormat('Y-m-d H:i:s',
      $request->date.' '.$request->endTime.':00')->toDateTimeString();

      //住所のジオコード(Googlemapの座標)を計算する
      if($request->address){
        $geocode = app('geocoder')->geocode($request->address)->all();
        $latitude = $geocode[0]->getLatitude();
        $longitude = $geocode[0]->getLongitude();
        // dd($geocode->all());
      }
      else{
        $latitude = 0;
        $longitude = 0;
      }
      //データ格納
      $plan->places()->create([
        'name' => $request->name,
        'start_time' => $startDateTime,
        'end_time' => $endDateTime,
        'location' => $request->address,
        'memo' => $request->memo,
        'latitude' => $latitude,
        'longitude' => $longitude,
      ]);

    // } catch(\ Exception $e) {
    //   echo('入力エラーです。戻るを押して再度入力してください。');
    //   exit();
    //   // return back()->withInput(); //とりあえずけしとく
    // }

    return back();
  }

//行き先の情報の更新
  public function update(Request $request, Plan $plan, Place $place)
  {
    // dd($request->startTime.':00');
    // dd($startTime = Carbon::parse($request->startTime.':00'));
    $startTime = Carbon::parse($request->startTime.':00');
    $endTime = Carbon::parse($request->endTime.':00');

    // dd(Carbon::createFromFormat('Y-m-d H:i:s',$request->startTime.':00')->toDateTimeString());

    // $startTime = Carbon::createFromFormat('Y-m-d H:i:s',
    // $request->startTime)->toDateTimeString();
    //
    // $endTime = Carbon::createFromFormat('Y-m-d H:i:s',
    // $request->endTime)->toDateTimeString();



     $place->find($place->id)->update([
       'name' => $request->placeName,
       'start_time' => $startTime,
       'end_time' => $endTime,
       'location' => $request->location,
       'memo' => $request->memo,
     ]);

     \Session::flash('flash_message',
      '「<strong>'.$request->placeName.'</strong>」の情報を更新しました');

     return back();
  }

  //行き先の削除
  public function destroy(Request $request, Plan $plan, Place $place)
  {
    // $this->authorize('destroy', $place);
    \Session::flash('flash_message',
     '「<strong>'.$place->name.'</strong>」を削除しました');
    $place->delete();
    return back();
  }
  public function viewMap(Request $request)
  {
    // $response = \GoogleMaps::load('geocoding')
    // ->setParam (['address' =>'santa cruz'])
    // ->get();
    //
    // dd($response);

    return view('map.viewMap');
  }

  public function getCoordinate(Plan $plan, Place $place){
    //住所のジオコード(Googlemapの座標)を計算する
    if($place->location){
      $geocode = app('geocoder')->geocode($place->location)->all();
      $latitude = $geocode[0]->getLatitude();
      $longitude = $geocode[0]->getLongitude();
      // dd($geocode->all());
    }
    else{
      $latitude = 0;
      $longitude = 0;
    }

    //データ格納
    $place->fill([
      'latitude' => $latitude,
      'longitude' => $longitude
    ]);
    $place->save();
    // dd($place);
    return back()->withInput();
  }

  // ルート描画
  public function drawRoute(Plan $plan, Place $place){
    return view('plan.tripRoutedraw', [
          'plan' => $plan,
          'places' => $this->places->forPlan($plan),
        ]);
  }

  //自動ルート探索・・スケジュールの順番ばらばらになるけど最短ルートを求める
  public function searchRoute(Plan $plan, Place $place){
    return view('plan.tripRoutedraw', [
          'plan' => $plan,
          'places' => $this->places->forPlan($plan),
        ]);
  }
}
