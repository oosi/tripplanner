<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\PlanRepository;
use App\Http\Requests;
use App\Plan;

class PlanController extends Controller
{
    /*
    * The Plan repository instance.
    *
    * @var PlanRepository
    */
    protected $plans;

    /**
    * Create a new controller instance.
    *
    * @return void
    */
    public function __construct(PlanRepository $plans)
    {
        $this->middleware('auth');
        $this->plans = $plans;
    }

    /**
    * Show the application dashboard.
    *
    * @return \Illuminate\Http\Response
    */

    public function index(Request $request) {
        // dd($request->user());
        return view('home', [
            'plans' => $this->plans->forUser($request->user()),
        ]);

    }


    public function add(Request $request) {
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);
        //入力を開始日・終了日に分割
        $date = explode(' to ',$request->date);
        if($date[1] == null){
            $date[1] = [0];
        }
        // dd($date);

        $request->user()->plans()->create([
            'name' => $request->name,
            'start_date' => $date[0],
            'end_date' => $date[1],
        ])->toSql();
        return redirect('/home');
    }

    public function destroy(Request $request, Plan $plan)
    {
        // dd($plan);
        // $this->authorize('destroy', $plan); //あとで追加する

        $plan->delete();

        return redirect('/home');
    }

}
